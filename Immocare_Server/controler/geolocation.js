var cities = require('cities');

/* Get value of Longitude from city's name */
function getLongitude(mycity)
{
    var result = cities.findByState(mycity);
    var longitude = result.longitude;

    return (longitude);
} exports.getLongitude = getLongitude; 

/* Get value of latitude from city's name */
function getLatitude(mycity)
{
    var result = cities.findByState(mycity);
    var latitude = result.latitude;

    return (latitude);
} exports.getLatitude = getLatitude;

/* Get city's name with Latitude/Longitude's values */
function getCityByGPS(latitude, longitude)
{
    var result = cities.gps_lookup(latitude, longitude);
    var city = result.city;

    return (city);
} exports.getCityByGPS = getCityByGPS;

/* Get city's name with zipcode's value */
function getCityByZipCode(zipcode)
{
    var result = cities.zip_lookup(zipcode);
    var city = result.city;

    return (city);
} exports.getCityByZipCode = getCityByZipCode;

/* Get Latitude's value with zipcode's value */
function getLatitudeByZipCode(zipcode)
{
    var result = cities.zip_lookup(zipcode);
    var latitude = result.latitude;

    return (latitude);
} exports.getLatitudeByZipCode = getLatitudeByZipCode;

/* Get Longitude's value with zipcode's value */
function getLongitudeByZipCode(zipcode)
{
    var result = cities.zip_lookup(zipcode);
    var longitude = result.longitude;

    return (longitude);
} exports.getLongitudeByZipCode = getLongitudeByZipCode;

/* Object Geolocation */
function GeolocationResult()
{
    this.myzipcode;
    this.mycity;
    this.myGeo;
    this.mylongitude;
    this.mylatitude;
}

/* Get informations of geolocation
    Return GeolocationResult if the system know the zipcode
    Return NULL else
*/
exports.setGeolocation = function(req)
{
    var result = new GeolocationResult();

    if (req.body.zipcode != null)
    {
        result.myzipcode = req.body.zipcode;
        result.mycity = getCityByZipCode(result.myzipcode);
        if (result.mycity != null)
        {
            result.mylongitude = getLongitudeByZipCode(result.myzipcode);
            result.mylatitude = getLatitudeByZipCode(result.myzipcode);
            result.myGeo = Math.abs(result.mylatitude) + Math.abs(result.mylongitude);
        }
        else
            return (null);
    }
    else
    {
        result.mylongitude = req.body.longitude;
        result.mylatitude = req.body.latitude;
        result.myGeo = Math.abs(result.mylatitude) + Math.abs(result.mylongitude);
        result.mycity = exports.getCityByGPS(result.mylatitude, result.mylongitude);      
    }
    return (result);
}