var user_model = require('../model/user_sql.js');

/* Control informations of user login 
URL Waited [POST]:
- http://localhost:3000/login

JSON Waited :
- email
- password
*/
exports.login = function(req, res){
  user_model.getUser(req, function(err, data){
    if (err) return res.status(500).send();
    else if (data == null) return res.status(404).send();
    req.session.user = data;
    return res.status(200).send();
  });
}

/* logout of user and delete informations of the current session 
URL Waited [POST]:
- http://localhost:3000/logout

JSON Waited :
- email
- password
*/
exports.logout = function(req, res){
  user_model.getUser(req, function(err, data){
    if (err) return res.status(500).send();
    else if (data == null) return res.status(404).send();
    if (!req.session.user) return res.status(401).send();
    req.session.user = null;
    return res.status(200).send();
  });
}

/* Create user with Control informations sent
URL Waited [POST]:
- http://localhost:3000/register

JSON Waited :
- firstname
- name
- email
- password
- zipcode
- (img)
- (type 0 -> locataire or 1 -> watcherman)

OR :
- firstname
- name
- email
- password
- latitude
- longitude
- (img)
- (type 0 -> locataire or 1 -> watcherman)
*/
exports.register = function(req, res){
 if (req.body.password == null || req.body.email == null)
    return res.status(400).send();
 if (req.body.zipcode == null)
    if (req.body.latitude == null || req.body.longitude == null)
        return res.status(400).send();
    
  user_model.checkUser(req, function(err, data){
    if (err) return res.status(500).send();
    else if (data != null) return res.status(401).send();
    else
      user_model.insert(req, function(err, data){
        if (err) return res.status(500).send();
        return res.status(200).send();
      });
  });
}

/* Get list of watchmans 
URL Waited [GET]:
- http://localhost:3000/watchmanlist
*/
exports.watchmanlist = function(req, res){
  user_model.getWatcherman(req, function(err, data){
    if (err) return res.status(500).send();
    return res.status(200).send(data);
  });
}

/* user Get his informations 
URL Waited [GET]:
- http://localhost:3000/myProfil
*/
exports.getProfil = function(req, res){
    var data = new user_model.Modelprofil();
    data.firstname = req.session.user.firstname;
    data.name = req.session.user.name;
    data.email = req.session.user.email;
    data.city = req.session.user.city;
    data.country = req.session.user.country;
    data.zipcode = req.session.user.zipcode;
    
    return res.status(200).send(data);
}
