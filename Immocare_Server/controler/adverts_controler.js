var adverts_model = require('../model/adverts_sql.js');

/* Get all adverts
URL Waited [GET]:
- http://localhost:3000/advertslist
*/
exports.list = function(req, res){
    adverts_model.getAll(function(err, data){
        if (err) return res.status(500).send();
        return res.status(200).send(data);
    });
}

/* Insert Advert in Database
URL Waited [POST]:
- http://localhost:3000/addadvert

(Need connexion of user) JSON Waited :
- title
- body
- (pictures)
*/
exports.addOne = function(req, res){
    var id_user = req.session.user.id;

    adverts_model.insert(id_user, req, function(err, data){
        if (err) return res.status(500).send();
        return res.status(data).send();
    });
}

/* Get informations of an advert 
URL Waited [GET]:
- http://localhost:3000/advert/(id of advert)
*/
exports.listOne = function(req, res){
    var id_advert = req.params.id;

    adverts_model.getOne(id_advert, function(err, data){
        if (err) return res.status(500).send();
        if (data == null) return res.status(404).send();
        return res.status(200).send(data);
    });
}

/* Edit informations of an advert
URL Waited [POST]:
- http://localhost:3000/editadvert/(id of advert)

(Need connexion of user) JSON Waited :
- title
- body
- (pictures)
*/
exports.editOne = function(req, res){
    var id_user = req.session.user.id;
    var id_advert = req.params.id;

    adverts_model.update(id_user, id_advert, req, function(err, data){
        if (err) return res.status(500).send();
        return res.status(data).send();
    });
}
