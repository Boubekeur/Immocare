var express = require('express');
var bodyParser = require('body-parser');
var session = require('express-session');
var app = express();
var user_sql = require('./controler/user_controler.js');
var adverts_sql = require('./controler/adverts_controler.js');
var cities = require('cities');

/* Manage User session */
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(session({secret:"ui2hf893hf232ofn3023fp", resave:false, saveUninitialized:true}));

/* Register user, work only if informations are corrects and dont exist in database */
app.post('/register', function (req, res) {
  user_sql.register(req, res);
});

/* login user */
app.post('/login', function (req, res) {
  user_sql.login(req, res);
});

/* logout user, work only if user is connected */
app.post('/logout', function (req, res) {
  user_sql.logout(req, res);
});

/* Get list of watchmans, work only if user is connected */
app.get('/watchmanlist', function(req, res){
  if (!req.session.user)
    return res.status(401).send();
  if (req.session.user.type != 0)
    return res.status(401).send();
  console.log(req.session.user.type);
  user_sql.watchmanlist(req, res);
});

/* Get list of adverts, work only if user is connected */
app.get('/advertslist', function(req, res){
  if (!req.session.user)
    return res.status(401).send();
  adverts_sql.list(req, res);
});

/* add an advert, work only if user is connected */
app.post('/addAdvert', function (req, res) {
  if (!req.session.user)
    return res.status(401).send();
  adverts_sql.addOne(req, res);
});

/* Get informations of an advert, work only if user is connected */
app.get('/advert/:id', function (req, res){
  if (!req.session.user)
    return res.status(401).send();
  adverts_sql.listOne(req, res);
});

/* Change informations of an advert, work only if user is connected and the advert selected belongs to him */
app.post('/editAdvert/:id', function (req, res) {
  if (!req.session.user)
    return res.status(401).send();
  adverts_sql.editOne(req, res);
});

/* Get his informations, work only if user is connected */
app.get('/myProfil', function (req, res){
  if (!req.session.user)
    return res.status(401).send();
  user_sql.getProfil(req, res);
});


/* Start the NodeJS Server on port 3000 */
app.listen(3000, function () {
  console.log('Immocare app listening on port 3000!');
});
