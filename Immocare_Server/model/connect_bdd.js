var mysql = require('mysql');

/* Singleton for Mysql Connexion */

var pool;
module.exports = {
    getPool: function () {
      if (pool) return pool;
      pool = mysql.createPool({
        host     : 'localhost',
        user     : 'root',
        password : '',
        database : 'immocare'
      });
      return pool;
    }
};
