var db = require('./connect_bdd.js');
var assets = require('./assets.js');
var geolocation = require('../controler/geolocation.js');
var pool = db.getPool();
var profile_img = assets.getProfileImg();

/* Model user */
function Modelprofil()
{
    this.email;
    this.firstname;
    this.name;
    this.country;
    this.city;
    this.zipcode;
}   exports.Modelprofil = Modelprofil;

/* SQL function for Select informations of user */
exports.getUser = function(req, callback){
    var myemail = req.body.email;
    var mypassword = req.body.password;

    pool.getConnection(function(err, con){
        var login_query = "SELECT * FROM users WHERE email="+ con.escape(myemail) +" and password="+ con.escape(mypassword) +";";
        con.query(login_query, function(err, result){
            if (err) callback(err, null);
            else callback(null, result[0]);
        });
    });
}

/* SQL function for check if user exist in database */
exports.checkUser = function(req, callback){
    var myemail = req.body.email;

    pool.getConnection(function(err, con){
        var login_query = "SELECT * FROM users WHERE email="+ con.escape(myemail) +";";
        con.query(login_query, function(err, result){
            if (err) callback(err, null);
            else callback(null, result[0]);
        });
    });
}

/* SQL function for Insert a new user in database with split JSON in the Sql query */
exports.insert = function(req, callback){
    var myemail = req.body.email;
    var mypassword = req.body.password;
    var resultGeoloc = geolocation.setGeolocation(req);
    var mytype = 0;
    var mycountry = "France";
    var myname = req.body.name;
    var myfirstname = req.body.firstname;

    if (resultGeoloc == null)
        callback(null, 488);
    if (req.body.img != null)
        profile_img = req.body.img;
    if (req.body.type != null)
        mytype = req.body.type;
    pool.getConnection(function(err, con){
        var register_query = "INSERT INTO users (country, type, name, firstname, email, password, img, city, Geo_longitude, Geo_latitude, MyGeolocation, zipcode) VALUES ("+ con.escape(mycountry) +" , "+ con.escape(mytype) +" , "+ con.escape(myname) +" , "+ con.escape(myfirstname) +" , "+ con.escape(myemail) +" , "+ con.escape(mypassword)+" , "+ con.escape(profile_img) +" , "+ con.escape(resultGeoloc.mycity) +" , "+ con.escape(resultGeoloc.mylongitude) +" , "+ con.escape(resultGeoloc.mylatitude) +" , "+ con.escape(resultGeoloc.myGeo) +" , "+ con.escape(resultGeoloc.myzipcode) +" );";

        con.query(register_query, function(err, result){
            if (err) callback(err, null);
            else callback(null, 200);
        });
    });
}

/* SQL function for Select 100 watcherman from database (change `limit` value for more or less results) */
exports.getWatcherman = function(req, callback){
    pool.getConnection(function(err, con){
        // Sorting query by Geolocation  
        var login_query = "SELECT id, name, firstname, city, country, note, zipcode, ABS(MyGeolocation - "+ req.session.user.MyGeolocation +") AS distance FROM users WHERE type = 1 ORDER BY distance LIMIT 100;";
        con.query(login_query, function(err, result){
            if (err) callback(err, null);
            else callback(null, result);
        });
    });
}
