var db = require('./connect_bdd.js');
var pool = db.getPool();
var asset = require('./assets.js');
var mypictures = asset.getPictureImg();

/* SQL function for get 15 adverts (Change the `limit` for more adverts) */
exports.getAll = function(callback){
    pool.getConnection(function(err, con){
    var list_query = "SELECT id, id_user, title, creation_date, pictures FROM adverts WHERE display=1 limit 15;";
    con.query(list_query, function(err, result){
      if (err)
        callback(err, null);
      callback(null, result);
    });
  });
}

/* SQL function for insert an advert in database with split Json in the Sql query */
exports.insert = function(id_user, req, callback){
    var mytitle = req.body.title;
    var mybody = req.body.body;
    if (req.body.pictures != null)
        mypictures = req.body.pictures;

    pool.getConnection(function(err, con){
      var check_query  = "SELECT * FROM adverts WHERE id_user="+id_user+" and title = ? ;";
      con.query(check_query, [mytitle], function(err, result, fields){
        if (err) callback(err, null);
        if (result.length == 0)
        {
          var addAdvert_query = "INSERT INTO adverts (id_user, title, body, pictures) VALUES ("+
            id_user + " , " + con.escape(mytitle) +" , "+ con.escape(mybody)+" , "+ con.escape(mypictures)+");";
          con.query(addAdvert_query,function(err, result){
            if (err) callback(err, null);
            else
            {
              var addContract_query = "INSERT INTO contracts (id_advert, id_vacationer) VALUES ("+
              result.insertId + " , " + id_user +");";
              con.query(addContract_query,function(err, result){
                if (err) callback(err, null);
                callback(null, 200);
              });
            }
          });
        }
        else
          callback(null, 401);
      });
    });
}

/* SQL function for select information of an advert in database */
exports.getOne = function(id_advert, callback){
  pool.getConnection(function(err, con){
    var list_query = "SELECT * FROM adverts WHERE id = "+con.escape(id_advert)+";";
    con.query(list_query, function(err, result){
      if (err) callback(err, null);
      callback(null, result[0]);
    });
  });
}

/* SQL function for update information of an advert in database */
exports.update = function(id_user, id_advert, req, callback){
  var mytitle = req.body.title;
  var mybody = req.body.body;
  var mypictures = req.body.pictures;

 pool.getConnection(function(err, con){
    var list_query = "SELECT * FROM adverts WHERE id = "+con.escape(id_advert)+";";
    con.query(list_query, function(err, result){
        if (err)  callback(err, null);
        if (mytitle == null)
          mytitle = result[0].title;
        if (mybody == null)
          mybody = result[0].body;
        if (mypictures == null)
          mypictures = result[0].pictures;
        if (result[0].id_user == id_user)
          {
            var edit_query = "UPDATE adverts SET `title` = "+ con.escape(mytitle) +", `body` = "+con.escape(mybody) +", `pictures` = "+
            con.escape(mypictures)+" WHERE id = "+con.escape(req.params.id)+";";
            con.query(edit_query, function(err, result){
            if (err) callback(err, null);
            callback(null, 200);
            });
          }
        else callback(null, 401);
      });
    });
}
